Titre : ER838

Objectif des programmes : Ce dossier fournit les programmes permettant de produire les illustrations et les chiffres contenus dans le texte de l'Études et résultats n° 838 de la DREES : "Les motivations de départ à la retraite : stabilité entre 2010 et 2012".

Lien vers l'étude : https://drees.solidarites-sante.gouv.fr/etudes-et-statistiques/publications/etudes-et-resultats/article/les-motivations-de-depart-a-la-retraite-stabilite-entre-2010-et-2012

Présentation de la DREES : La Direction de la recherche, des études, de l'évaluation et des statistiques (DREES) est le service statistique ministériel des ministères sanitaires et sociaux, et une direction de l'administration centrale de ces ministères.
https://drees.solidarites-sante.gouv.fr/article/presentation-de-la-drees

Source de données : vague 2 de l'enquête sur les motivations de départ à la retraite, DREES-Cnav-DSS. Traitements : Drees.

Les programmes ont été exécutés pour la dernière fois avec le logiciel R version 3.6.1, le 15/01/2021.
